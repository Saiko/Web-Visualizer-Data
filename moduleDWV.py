
class DWVEntity:
    def __init__(self, type: str, name: str) -> None:
        self.name = name or ''
        self.type = type or ''
        self.label = ''
        self.note = ''
        self.linkLabel = ''
    
    def toDict(self) -> dict:
        return {
            "linkLabel": self.linkLabel,
            "label": self.label,
            "name": self.name,
            "note": self.note,
            "type": self.type
        }
    
    def setLinkLabel(self, label: str):
        self.linkLabel = label
        return self
    
    def setNote(self, note: str):
        self.note = note
        return self
    
    def setLable(self, label: str): 
        self.label = label
        return self
    

class DWVModule:
    def __init__(self) -> None:
        self.entities = []
        
    def addEntity(self, type: str, name):
        entity = DWVEntity(type, name)
        self.entities.append(entity)
        return entity
        
    def getResults(self) -> dict:
        return {
            "entities": [entity.toDict() for entity in self.entities]
        }