import configparser
from importlib.machinery import SourceFileLoader
import sys
import os
import subprocess
import xml.etree.ElementTree as xml
import re
import shlex

# TODO Выявить баг с NoneType и методом get в runMaltegoTransform
# TODO Исправить перезагрузку конфигов модулей
# TODO Рефакторинг 

DWSmodules = {}

config_file = 'modules.ini'

config = configparser.ConfigParser()
config.read(config_file)

def getModule(name: str): 
    return sys.modules[name+'_module']
    
    
def importConfigModule(name: str):
    
    moduleConfig = config[name]
    
    if moduleConfig == None: return None
    
    path = moduleConfig['path']
    
    module = SourceFileLoader(name+'_module', os.path.normpath(path)).load_module()
    
    return module

def xml_to_dict(xml_string: str) -> dict:
    
    def xml_process(parent: xml.Element) -> dict:
        if len(parent) == 0: return parent.text
        res = {}
        for child in parent:
            if child.tag in res:
                if type(res[child.tag]) == dict:
                    res[child.tag] = [ res[child.tag] ]
                x = xml_process(child)
                for key in child.attrib:
                    if type(x) == dict:
                        x['_'+key] = child.attrib[key]
                if type(res[child.tag]) == list:
                    res[child.tag].append(x)
            else:
                res[child.tag] = xml_process(child)
                
            for key in child.attrib:
                if type(res[child.tag]) == dict:
                    res[child.tag]['_'+key] = child.attrib[key]
        return res
    
    root = xml.fromstring(xml_string)
    return xml_process(root)


def parseMaltegoResult(result:dict): 
    
    entities_raw = result.get('Entities', {}).get('Entity', [])
    entities = []
    
    for entity in entities_raw:
        entity_dict = {}
        entity_dict['name'] = entity['Value']
        entity_dict['weight'] = entity['Weight']
        entity_dict['type'] = entity['_Type']
        # entity_dict['additionalfields'] = {
        #     entity.get('AdditionalFields', [{}])[0]['Field'][0].lower(): entity['AdditionalFields'][0]['Field'][0]
        # }
        entities.append(entity_dict)
    
    return {
        'entities': entities        
    }

def runMaltegoTransform(name: str, data_input = ''):
    moduleConfig = DWSmodules.get(name)
    if not moduleConfig: return None
    if not moduleConfig.get('maltego'): return None
    
    params = shlex.split(moduleConfig.get('params', ''))
    process = subprocess.Popen([moduleConfig.get('cmd', '')] + params + [data_input], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    
    # NOTE: Генерируемая команда в Maltego: {cmd} {params} "<Entity name>" "field1=field1 value#field2=field2 value"
    # NOTE: Задача понять для чего используется последний аргумент
    
    output, errout = process.communicate()

    if errout: raise Exception(errout.decode())

    output = output.decode()
    match = re.search(r"\<MaltegoMessage\>\s*(.*?)\s*<\/MaltegoMessage\>", output, flags=(re.S))
    if not match: return None
    output = match.group(1)
    
    res = xml_to_dict(output)
    res = parseMaltegoResult(res)

    return res
    


def parseModules():
    DWSmodules.clear()
    config.read(config_file)
    for name in config.sections():
        module = config[name]
        if not module.get('path') and not module.get('maltego'): continue
        
        DWSmodules[name] = {
            "name": name,
            "cmd": module.get('cmd'),
            "path": module.get('path'),
            "params": module.get('params'),
            "input_type": module.get('input_type'),
            "maltego": module.getboolean('maltego')
        }

