from flask import Flask, send_file, request

import moduleParser


app = Flask(__name__)

@app.route('/')
def index():
    return send_file('./pages/index.html')

@app.route('/modules')
def modules():
    moduleParser.parseModules()
    return [m[1] for m in moduleParser.DWSmodules.items()]

@app.route('/module/<name>')
async def runModule(name):
    moduleParser.parseModules()
    module = moduleParser.DWSmodules.get(name)
    
    if not module: return {
        "error": "No module"
    }
    
    result = None
    
    if module.get('maltego'): 
    
        params = request.args.get('params')

        try:
            result = moduleParser.runMaltegoTransform(name, params)
        except Exception as err:
            return {
                "error": err.args[0]
            }, 400
        
    else: 
        module = moduleParser.importConfigModule(name)
        if not module: return {
            "error": "No module"
        }, 400
        
        params = request.args.get('params')
        args = None if not params else params.split(' ')
        
        result = module.run(args)
    
    
    if not result: return {
        "error": "No result"
    }, 400
    
    return {
        "ok": True,
        "data": result
    }

if __name__ == '__main__':
    app.run(port=9999)