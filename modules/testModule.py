from moduleDWV import DWVModule

def run(args):
    
    dwv = DWVModule()
    
    dwv.addEntity('entity', 'Hello')
    dwv.addEntity('entity', 'World')
    dwv.addEntity('entity', '1')
    dwv.addEntity('entity', '2')
    dwv.addEntity('entity', '3')
    dwv.addEntity('entity', '4')
    dwv.addEntity('entity', '5')
    dwv.addEntity('entity', '6')
    dwv.addEntity('entity', '7')
    
    return dwv.getResults()