

function createNotificationElement() {
    let block = document.createElement('div');
    block.classList.add('notification');
    return block;
}

function createNotification(text, styledClassName = null) {
    let el = createNotificationElement();
    if (styledClassName) el.classList.add(styledClassName);
    el.textContent = text;
    document.body.append(el);
    let style = getComputedStyle(el);
    let time = parseInt(style.getPropertyValue('--timer-time'));
    if (time) setTimeout(_ => el.remove(), time*1000);
}

function createNotificationError(text) {
    createNotification(text, 'notification_error');
}

function createNotificationInfo(text) {
    createNotification(text, 'notification_info');
}