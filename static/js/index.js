
// TODO Создать окно ифнормации об Entity
// TODO Окно редактирования списка моделей


let drag = null;
let dragX = null;
let dragY = null;

let linkingEntity = null;
let linkingRemove = false;

let contextEntity = null;
let contextMenu = null;

let dwv = new DWV();
let dwvDragEl = null;

let autoConstruct = null;

document.onmouseup = _ => {
    if (drag && drag.id === "lineel") {

    }

    drag = null;
    dwvDragEl = null;
};
document.onmousemove = e => {
    if (
        e.buttons === 1 && 
       (e.target.classList.contains('draggable') || drag) && 
       (dwvDragEl = dwv.getEntityByElement(drag || e.target)) &&
       !linkingEntity
    ) {
        if (!drag) {
            drag = e.target;
            dragX = e.pageX-e.target.offsetLeft;
            dragY = e.pageY-e.target.offsetTop;
        }

        Object.assign(drag.style, {
            position: 'absolute',
            userSelect: "none",
            left: e.x-dragX+'px',
            top: e.y-dragY+'px'
        });

        dwvDragEl.pos.x = (e.x-dragX)/dwv.viewport.z-dwv.viewport.x;
        dwvDragEl.pos.y = (e.y-dragY)/dwv.viewport.z-dwv.viewport.y;

        dwv.renderLines();
        dwv.render();

    } else if (linkingEntity) {
        dwv.renderLine(linkingEntity.el.offsetLeft+(linkingEntity.el.offsetWidth/2), linkingEntity.el.offsetTop+(linkingEntity.el.offsetHeight/2), e.pageX, e.pageY, {
            color: linkingRemove ? 'red' : 'black'
        });
    } else if (e.buttons === 1 && e.target.id === "lineel") {
        if (!drag) {
            drag = e.target;
            dragX = (e.pageX-e.target.offsetLeft)/dwv.viewport.z-dwv.viewport.x;
            dragY = (e.pageY-e.target.offsetTop )/dwv.viewport.z-dwv.viewport.y;
        }
        dwv.viewport.x = (e.x/dwv.viewport.z-dragX);
        dwv.viewport.y = (e.y/dwv.viewport.z-dragY);
        dwv.renderLines();
        dwv.render();
    }
}
window.addEventListener('click', e => {
    if (linkingEntity) {
        if (e.target.id === 'lineel') {
            linkingEntity = null;
            dwv.renderLines();
            return;
        }
        let entity = dwv.getEntityByElement(e.target);
        if (!entity || entity.id === linkingEntity.id) return;

        linkingEntity.linkTo(entity);
        linkingEntity = null;
        dwv.renderLines();
    }
});




document.addEventListener('DOMContentLoaded', _ => {
    contextMenu = document.getElementById('contextMenu');

    window.oncontextmenu = e => {
    
        const contextMenu = document.getElementById('contextMenu');
        if (contextMenu) {
            if (e.target.classList.contains('entity')) {
                e.preventDefault();
        
                Object.assign(contextMenu.style, {
                    left: e.pageX+"px",
                    top: e.pageY+"px"
                });
        
                contextEntity = dwv.getEntityByElement(e.target);
        
                contextMenu.classList.remove('invisible');
            } else if (!contextMenu.classList.contains('invisible')) {
                closeContextMenu();
            }
        }
    
    }
    
    window.onmousedown = e => {
        
        if (e.buttons === 1 &&
            (e.target == dwv.lineEl || e.target.classList.contains('entity'))
            ) {
            e.preventDefault();
    
            closeContextMenu();
        }
    
    }
})


function setLinking(entity) {
    linkingEntity = entity;
}

function closeContextMenu() {
    if (!contextMenu) return;
    invisible(contextMenu);
    document.querySelectorAll('.subContextMenu').forEach(v => invisible(v))
}

function invisible(el) {
    if (!el) return;
    if (!el.classList.contains('invisible'))
            el.classList.add('invisible');
}
function uninvisible(el) {
    if (!el) return;
    if (el.classList.contains('invisible'))
            el.classList.remove('invisible');
}

function addEntity(name = 'Entity') {
    let entity = dwv.addEntity('entity', name);
    entity.pos.x = entity.pos.x - dwv.viewport.x + window.innerWidth/2;
    entity.pos.y = entity.pos.y - dwv.viewport.y + window.innerHeight/2;
    dwv.render();
    dwv.renderLines();
    return entity;
}

function removeEntityByElement(element) {
    if (!element) return;
    let entity = dwv.getEntityByElement(element);
    if (!entity) return;
    dwv.removeEntity(entity);
    dwv.render();
    dwv.renderLines();
}

function removeEntity(entity) {
    if (!entity) return;
    dwv.removeEntity(entity);
    dwv.render();
    dwv.renderLines();
}

function constructTree() {
    autoConstruct = constructTree;
    dwv.constructTree();
    dwv.render();
    dwv.renderLines();
}

function constructCyrcle() {
    autoConstruct = constructCyrcle;
    dwv.constructCyrcle();
    dwv.render();
    dwv.renderLines();
}

async function runModuleForEntity(module, rootEntity) {
    let res = await runModule(module.name, rootEntity.name)

    if (!res) return;
    if (!res.ok && res.error) {
        createNotificationError(res.error);
        return;
    };

    for (let entity of res.data.entities) {
        let e = dwv.addEntity(entity.type, entity.name);
        e.setLabel(entity.label);
        rootEntity.linkTo(e);
    }
    dwv.render();
    dwv.renderLines();
    if (autoConstruct) autoConstruct();

    createNotificationInfo('Success!')
}

async function setContextModules() {
    let modulesEl = document.getElementById('contextModules');
    modulesEl.innerHTML = '';
    let els = [];

    let modules = await getModules();
    for (const module of modules) {
        let el = document.createElement('div');
        let span = document.createElement('span');
        el.classList.add('option')
        el.onclick = async _ => {
            closeContextMenu();
            let contextEntityCopy = dwv.getEntityByElement(contextEntity.el);
            runModuleForEntity(module, contextEntityCopy);
        }
        span.textContent = module.name;
        el.append(span);

        els.push(el);
    }

    modulesEl.append(...els);
}

// !async function() {
//     let e = addEntity()
//     let modules = await getModules();
//     await runModuleForEntity(modules[2], e)
//     constructCyrcle();
// }();