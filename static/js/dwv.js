let tree = document.getElementById("tree");
let selectedEntity = null;




// TODO Сделать скроллирнг от точки где находится мышь
// TODO Сделать так, чтобы узлы не клались друг на друга в constructTree и constructCyrcle
// TODO Сделать построение узлов по кругу
// TODO Замаппить энтити


class Vector2 {
    constructor(x, y) {
        this.x = x || 0;
        this.y = y || 0;
    }
}
function vec2(x,y){return new Vector2(x,y)}

class DWVEntity {
    constructor(type, name) {
        this.name = name || 'Entity';
        this.type = type || 'Entity';
        this.note = '';
        this.label = '';
        this.linkLabel = '';

        ////  RENDER DATA  ////
        this.pos = vec2();
        this.el = null;
        ///////////////////////

        this.id = (Math.random()*Date.now()).toString(36).replace('.');
        this.createdAt = Date.now();
        this.links = [];
    }

    removeElement() {
        this.el.remove();
        this.links.forEach(v => {
            v.links = v.links.filter(l => l.id !== this.id);
        });
    }

    addLink(entity) {
        if (!(entity instanceof DWVEntity)) throw new Error('Entity is not a DWVEntity');
        if (this.links.indexOf(entity) > -1) return;
        this.links.push(entity);
    }

    linkTo(entity) {
        if (!(entity instanceof DWVEntity)) throw new Error('Entity is not a DWVEntity');
        this.addLink(entity);
        entity.addLink(this);
    }

    setNote(note) {
        this.note = note;
    }

    setLinkLabel(name) {
        this.linkLabel = name;
    }

    setLabel(name) {
        this.label = name;
    }
}

class DWV {
    constructor() {

        this.entities = [];

        this.viewport = {
            x: 0,
            y: 0,
            z: 1 // zoom 
        };

        this.lineWidth = 2;
        this.lineColor = 'black';


        this.lineEl = document.createElement('canvas');
        this.lineEl.id = "lineel";
        this.lineCtx = this.lineEl.getContext('2d');

        document.body.append(this.lineEl);

        const scrollMin = 0.25;
        const scrollMax = 4;
        const scrollSpeed = 1.5;

        this.scrollPos = vec2();
        window.addEventListener('wheel', e => {
            this.scrollPos.x = e.pageX;
            this.scrollPos.y = e.pageY;
            let z = this.viewport.z - e.deltaY*scrollSpeed/2000*this.viewport.z;
            this.viewport.z = z < scrollMin ? scrollMin : z > scrollMax ? scrollMax : z;
            this.render();
            this.renderLines();
        });

        new ResizeObserver(_ => this.updateCanvasOffset()).observe(document.body);
        this.updateCanvasOffset();

        this.renderLines()
    }

    getEntityByElement(element) {
        return this.entities.filter(v => v.el == element)[0];
    }

    addEntity(type, name) {
        let entity = new DWVEntity(type, name);

        this.entities.push(entity);

        return entity;
    }

    removeEntity(entity) {
        entity.removeElement();
        this.entities = this.entities.filter(v => v.id !== entity.id);
    }

    linkEntities(entity1, entity2) {
        if (!(entity1 instanceof DWVEntity) ||
            !(entity2 instanceof DWVEntity)) throw new Error('Entity is not a DWVEntity');
        entity1.linkTo(entity2);
    }

    constructTree() {
        this.constructNodes((root, node, i) => {
            const space = 100;
            const count = root.links.length;
            const maxColumns = count/3|0;
            const level = (((i + maxColumns) / maxColumns) | 0);
            const x = root.pos.x + ((i % maxColumns) - (count > maxColumns ? maxColumns : count) / 2) * (space + level) + space / 2;
            const y = root.pos.y + space * level;
            
            node.pos.x = x;
            node.pos.y = y;
        });
    }

    constructNodes(construct) {
        const trees = new Set();
        const visited = new Set();
        function counterProcess(node, oldest) {
            if (visited.has(node.id)) return;
            visited.add(node.id);

            node.links.forEach(child => {
                if (oldest.createdAt > child.createdAt) oldest = child;
                let oldestNode = counterProcess(child, oldest);
                if (oldestNode && oldest.createdAt > oldestNode.createdAt) oldest = oldestNode;
            });
            return oldest;
        }
        this.entities.forEach(entity => {
            let root = counterProcess(entity, entity);
            if (root) trees.add(root);
        });

        trees.forEach(root => {
            const visited = new Set();
            const processNode = (node) => {
                if (visited.has(node.id)) {
                    return;
                }
                visited.add(node.id);
                node.links.forEach((child, i, links) => {
                    if (!visited.has(child.id)) construct(node, child, i);
                    processNode(child);
                });
            }
            processNode(root);
        });
    }

    constructCyrcle() {
        this.constructNodes((root, node, i) => {
            const space = 150;
            const maxCount = 12;
            const count = root.links.length;
            const level = Math.floor((count / maxCount) + 1);
            const startAngle = 0;
            const endAngle = 360;
            const angle = startAngle+(endAngle)/count*i;
            const multSpace = space*(i%level);
            const x = root.pos.x + (space + multSpace) * Math.cos(angle * Math.PI/180);
            const y = root.pos.y + (space + multSpace) * Math.sin(angle * Math.PI/180);
            
            node.pos.x = x;
            node.pos.y = y;
        });
    }

    render() {

        this.entities.forEach(e => {
            if (!e.el) {
                e.el = document.createElement('div');
                e.el.textContent = e.name;
                document.body.append(e.el);
                e.el.classList.add('entity', 'draggable', e.type);
            }
            let el = e.el;

            let screenPos = vec2(e.pos.x+this.viewport.x, e.pos.y+this.viewport.y);

            Object.assign(el.style, {
                left: screenPos.x*this.viewport.z+'px',
                top: screenPos.y*this.viewport.z+'px',
                scale: this.viewport.z+0.1
            });

        })

    }

    renderGrid() {
        if (!this.lineCtx) return;
        this.lineCtx.beginPath();
        let size = 50*this.viewport.z;
        
        
        this.lineCtx.strokeStyle = 'rgba(0,0,0,0.2)';
        this.lineCtx.lineWidth = 2 + 'px';

        for (let x = this.viewport.x*this.viewport.z % size; x < this.lineEl.width; x += size) {
            this.lineCtx.moveTo(x, 0);
            this.lineCtx.lineTo(x, this.lineEl.height);
        }
        for (let y = this.viewport.y*this.viewport.z % size; y < this.lineEl.height; y += size) {
            this.lineCtx.moveTo(0, y);
            this.lineCtx.lineTo(this.lineEl.width, y);
        }
        this.lineCtx.closePath();
        this.lineCtx.stroke();
    }

    // Нужна оптимизация
    renderLine(x1,y1,x2,y2, opt = {}) {
        this.renderLines() // Эта функция выполняет очистку холста и рисование линий, что означает, что так просто перерисовывать одну линию будет трудно.
        
        this.lineCtx.strokeStyle = opt.color || this.lineColor;
        this.lineCtx.beginPath();
        this.lineCtx.moveTo(x1, y1);
        this.lineCtx.lineTo(x2, y2);
        this.lineCtx.closePath();
        this.lineCtx.stroke();
    }

    renderLines() {

        let entities = this.entities;
        let w = this.lineEl.width;
        let h = this.lineEl.height;

        this.lineCtx.clearRect(0,0,w,h);

        this.renderGrid();

        this.lineCtx.lineWidth = this.lineWidth + 'px';
        this.lineCtx.strokeStyle = this.lineColor;

        this.lineCtx.beginPath();
        for (const entity of entities) {
            let p = entity.pos;
            for (const entityLink of entity.links) {
                let pl = entityLink.pos;
                this.lineCtx.moveTo((p.x+this.viewport.x)*this.viewport.z+(entity.el.offsetWidth/2), (p.y+this.viewport.y)*this.viewport.z+(entity.el.offsetHeight/2));
                this.lineCtx.lineTo((pl.x+this.viewport.x)*this.viewport.z+(entityLink.el.offsetWidth/2), (pl.y+this.viewport.y)*this.viewport.z+(entityLink.el.offsetHeight/2));
            }
        }
        this.lineCtx.closePath();
        this.lineCtx.stroke();
    }

    updateCanvasOffset() {
        let el = document.getElementById('lineel');
        if (el) {
            el.width = el.offsetWidth;
            el.height = el.offsetHeight;
        }
        this.renderLines();
    }

}


function isPointWithinElement(el, x, y) {
    const rect = el.getBoundingClientRect();
    const vertInView = (rect.top <= y) && ((rect.top + rect.height) >= y);
    const horInView = (rect.left <= x) && ((rect.left + rect.width) >= x);
    return (vertInView && horInView);
}

function isVisibleElement(el) {
    const rect = el.getBoundingClientRect();
    const windowHeight = window.innerHeight || document.documentElement.clientHeight;
    const windowWidth = window.innerWidth || document.documentElement.clientWidth;
    const vertInView = (rect.top <= windowHeight) && ((rect.top + rect.height) >= 0);
    const horInView = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);
    return (vertInView && horInView);
}

