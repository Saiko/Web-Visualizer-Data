
function call(method, path, options = {}) {
    path = path.replace(/^[\/\\]/, "");

    let url = location.protocol+'//'+location.host;

    return fetch(url + '/' + path, {
        method: method || "GET",
        ...options
    });
}

async function getModules() {
    return call('GET', '/modules').then(r => r.json());
}

function runModule(name, params) {
    return call('GET', `/module/${name}${params ? `?params=${params}` : ''}`).then(r => r.json());
}